package ftn.post_ms.service.impl;

import java.util.List;

import ftn.post_ms.entity.Post;
import ftn.post_ms.repository.PostRepository;
import ftn.post_ms.service.PostService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.orm.hibernate5.LocalSessionFactoryBean;
import org.springframework.stereotype.Service;



@Service
public class PostServiceImpl  implements PostService {

	@Autowired
	private PostRepository postRepository;



	@Override
	public  List<Post> findPosts() {
		return postRepository.findAll();
	}

	@Override
	public List<Post> findMyPosts(Integer userId) {
		return postRepository.findAllByUserId(userId);
	}
}
