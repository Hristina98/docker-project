package ftn.post_ms.service;

import java.util.List;

import ftn.post_ms.entity.Post;

public interface PostService {
	
	
	 List<Post> findPosts();

    List<Post> findMyPosts(Integer userId);
}
