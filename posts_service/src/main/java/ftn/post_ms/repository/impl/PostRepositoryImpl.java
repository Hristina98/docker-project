package ftn.post_ms.repository.impl;

import ftn.post_ms.entity.Post;
import org.springframework.stereotype.Component;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import java.util.List;

@Component
public class PostRepositoryImpl {
    @PersistenceContext
    private EntityManager entityManager;

    @SuppressWarnings("unused")
    public List<Post> findAllByUserId(Integer userId) {
        String hql = "SELECT p FROM Post p WHERE p.userId = :userID";
        TypedQuery<Post> query = entityManager.createQuery(hql, Post.class);
        query.setParameter("userID", userId);

        System.out.println(query.getResultList());

        return query.getResultList();
    }

}
