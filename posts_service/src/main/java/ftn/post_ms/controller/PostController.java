package ftn.post_ms.controller;

import java.util.List;

import ftn.post_ms.entity.Post;
import ftn.post_ms.service.PostService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;

import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


@RestController
@CrossOrigin
public class PostController {
	
	
	@Autowired
	private PostService postService;
	
	@RequestMapping(value = "/post")
	public ResponseEntity<?> findAllPosts() {
			
		return new ResponseEntity< List<Post>>(postService.findPosts(), HttpStatus.OK);

	}

	@RequestMapping(path="/myPosts/{userId}")
	public ResponseEntity<?> findMyPosts(@PathVariable("userId") Integer userId) {

		return new ResponseEntity< List<Post>>(postService.findMyPosts(userId), HttpStatus.OK);

	}
	

}
