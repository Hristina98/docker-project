package com.example.demo.controller;

import com.example.demo.entity.User;
import com.example.demo.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;


import org.springframework.http.MediaType;

@RestController
@CrossOrigin
public class UserController {

    @Autowired
    private UserService userService;

    @RequestMapping(value = "/login/{username}/{password}")
    public ResponseEntity<?> findUser(@PathVariable("username") String username,
                                      @PathVariable("password") String password) {

        return new ResponseEntity<>(userService.findUser(username, password), HttpStatus.OK);
    }

    @RequestMapping(value = "/signup/addUser/", method = RequestMethod.POST,consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> addUser(@RequestBody User user) {

        return new ResponseEntity<>(userService.addUser(user), HttpStatus.OK);
    }

    @RequestMapping(value = "/findUser/{userId}")
    public ResponseEntity<?> findUser(@PathVariable("userId") Long userId) {

        return new ResponseEntity<>(userService.findUser(userId), HttpStatus.OK);
    }



}

