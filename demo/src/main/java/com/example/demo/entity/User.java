package com.example.demo.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;


@Entity
public class User implements Serializable {

    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    private Long id;
    private String username;
    private String password;
    private String fullName;
    private String email;
    private String mobile;
    private String gender;
    private Date birthDate;
    private String website;
    private String biography;
    private String role;
    private Boolean isPublic;
    private Boolean receiveMessages;
    private Boolean enableTags;
    private Boolean verified;


    public User() {

    }

    public User(Long id, String username, String password, String fullName, String email, String mobile,
                String gender, Date birthDate, String website, String biography, String role, Boolean isPublic,
                Boolean receiveMessages, Boolean enableTags, Boolean verified) {
        super();
        this.id = id;
        this.username = username;
        this.password = password;
        this.fullName = fullName;
        this.email = email;
        this.mobile = mobile;
        this.gender = gender;
        this.birthDate = birthDate;
        this.website = website;
        this.biography = biography;
        this.role = role;
        this.isPublic = isPublic;
        this.receiveMessages = receiveMessages;
        this.enableTags = enableTags;
        this.verified = verified;
    }

    public Long getId() {
        return id;
    }
    public void setId(Long id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }
    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }
    public void setPassword(String password) {
        this.password = password;
    }

    public String getRole() {
        return this.role;
    }
    public void setRole(String role) {
        this.role = role;
    }

    public String getFullName() {
        return fullName;
    }
    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getEmail() {
        return email;
    }
    public void setEmail(String email) {
        this.email = email;
    }

    public String getMobile() {
        return mobile;
    }
    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getGender() {
        return gender;
    }
    public void setGender(String gender) {
        this.gender = gender;
    }

    public Date getBirthDate() {
        return birthDate;
    }
    public void setBirthDate(Date birthDate) {
        this.birthDate = birthDate;
    }

    public String getWebsite() {
        return website;
    }
    public void setWebsite(String website) {
        this.website = website;
    }

    public String getBiography() {
        return biography;
    }
    public void setBiography(String biography) {
        this.biography = biography;
    }

    public Boolean getIsPublic() {
        return isPublic;
    }
    public void setIsPublic(Boolean isPublic) {
        this.isPublic = isPublic;
    }

    public Boolean getReceiveMessages() {
        return receiveMessages;
    }
    public void setReceiveMessages(Boolean receiveMessages) {
        this.receiveMessages = receiveMessages;
    }

    public Boolean getEnableTags() {
        return enableTags;
    }
    public void setEnableTags(Boolean enableTags) {
        this.enableTags = enableTags;
    }

    public Boolean getVerified() {
        return verified;
    }
    public void setVerified(Boolean verified) {
        this.verified = verified;
    }
}
