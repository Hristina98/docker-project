package com.example.demo.service;


import com.example.demo.entity.User;

import java.util.Optional;

public interface UserService {

    User findUser(String username, String password);

    User addUser(User user);

    Optional<User> findUser(Long userId);
}
